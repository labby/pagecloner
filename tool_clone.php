<?php

/**
 *  @module         pagecloner
 *  @version        see info.php of this module
 *  @authors        John Maats - Dietrich Roland Pehlke - Stephan Kuehn - vBoedefeld, cms-lab
 *  @copyright      2006-2010 John Maats - Dietrich Roland Pehlke - Stephan Kuehn - vBoedefeld
 *  @copyright      2010-2023 cms-lab
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$pagetodo = isset($_POST['pagetoclone']) ? (int) $_POST['pagetoclone'] : 0;

if(0 === $pagetodo) 
{
	die("no! [1]");	
}

// check if specified page exists in the database
$aSourcePageInfo = [];
$database->execute_query(
    "SELECT * FROM `".TABLE_PREFIX."pages` WHERE `page_id` = ".$pagetodo,
    true,
    $aSourcePageInfo,
    false
);

if (empty($aSourcePageInfo))
{
    header("Location: ".ADMIN_URL ."/admintools/tool.php?tool=pagecloner");
    die();
}

LEPTON_handle::register("page_tree");
$all_pages = [];
page_tree( 0, $all_pages );

    $aPageValues = [
        'LEPTON_URL' => LEPTON_URL,
        'IMAGE_URL' => (DEFAULT_THEME =="algos" ) ? LEPTON_URL.'/templates/algos/images' : LEPTON_URL.'/modules/lib_lepton/backend_images',		
        'leptoken' => get_leptoken(),
        'MOD_PAGECLONER' => pagecloner::getInstance()->language,	
        'all_pages' => $all_pages,
        'new_page_name' => $aSourcePageInfo['page_title']." copy",
        'source_page'   => $aSourcePageInfo
    ];
    
    $oTwig = lib_twig_box::getInstance();
    $oTwig->registerModule("pagecloner");
    echo $oTwig->render(
        "@pagecloner/modify_clone_settings.lte",
        $aPageValues
    );

$admin->print_footer();
