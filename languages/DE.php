<?php

/**
 *  @module         pagecloner
 *  @version        see info.php of this module
 *  @authors        John Maats - Dietrich Roland Pehlke - Stephan Kuehn - vBoedefeld, cms-lab
 *  @copyright      2006-2010 John Maats - Dietrich Roland Pehlke - Stephan Kuehn - vBoedefeld
 *  @copyright      2010-2023 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *
 */

// German language file for the admin module 'pagecloner'

$MOD_PAGECLONER = array (
	'INTRO_TEXT'		=> 'Mit diesem Addon kannst du Seiten kopieren.<br />Nachfolgend eine Liste aller vorhandenen Seiten:',
	'CHOOSE_PAGE'		=> 'Wähle eine zu kopierende Seite',
	'CLONE_PAGE'		=> 'Klick um die derzeitige Seite zu kopieren!',
	'CLONE_PAGETO'		=> 'Kopiere aktuelle Seite zu: ',
	'CLONE_FROM'		=> 'Kopieren: ',
	'CLONE_TO'			=> 'Zu neuer Seite: ',
	'ADD'				=> 'Kopieren',
	'CONTINUE'			=> 'Fortfahren',
	'ABORT'				=> 'Abbrechen',
	'CREATED'			=> 'Erfolgreich kopiert',
	'INCLUDE_SUBS'    	=> 'einschließlich Unterseiten'
);

