<?php

/**
 *  @module         pagecloner
 *  @version        see info.php of this module
 *  @authors        John Maats - Dietrich Roland Pehlke - Stephan Kuehn - vBoedefeld, cms-lab
 *  @copyright      2006-2010 John Maats - Dietrich Roland Pehlke - Stephan Kuehn - vBoedefeld
 *  @copyright      2010-2023 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *
 */

// English language file for the admin module 'pagecloner'

$MOD_PAGECLONER = array (
	'INTRO_TEXT'		=> 'This addon allows you to duplicate one page to a new page.<br />Below is a list of the pages that are installed on your site.',
	'CHOOSE_PAGE'		=> 'Choose a page to duplicate',
	'CLONE_PAGE'		=> 'Click to duplicate current page!',
	'CLONE_PAGETO'		=> 'Duplicate current page to: ',
	'CLONE_FROM'		=> 'Duplicating: ',
	'CLONE_TO'			=> 'To the new page: ',
	'ADD'				=> 'Duplicate',
	'CONTINUE'			=> 'Continue',
	'ABORT'				=> 'Cancel',
	'CREATED'			=> 'Successfully duplicated',
	'INCLUDE_SUBS'     	=> 'include subpages'
);
