<?php

/**
 *  @module         pagecloner
 *  @version        see info.php of this module
 *  @authors        John Maats - Dietrich Roland Pehlke - Stephan Kuehn - vBoedefeld, cms-lab
 *  @copyright      2006-2010 John Maats - Dietrich Roland Pehlke - Stephan Kuehn - vBoedefeld
 *  @copyright      2010-2023 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *
 */

class pagecloner extends LEPTON_abstract
{

    public static $instance;


    public function initialize() 
	{

    }
}