<?php

/**
 *  @module         pagecloner
 *  @version        see info.php of this module
 *  @authors        John Maats - Dietrich Roland Pehlke - Stephan Kuehn - vBoedefeld, cms-lab
 *  @copyright      2006-2010 John Maats - Dietrich Roland Pehlke - Stephan Kuehn - vBoedefeld
 *  @copyright      2010-2023 cms-lab 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

if( (isset($_POST['pagecloner_job'])) && ($_POST['pagecloner_job'] == "display_details") )
{
    require __dir__."/tool_clone.php";
    die();
}

LEPTON_handle::register("page_tree");
$all_pages = array();
page_tree(0, $all_pages);

// Generate pages list
if($admin->get_permission('pages_view') === true) {
	
	$pagecloner_vars = array(
		'THEME_URL'	=> THEME_URL,
		'LEPTON_URL' => LEPTON_URL,
		'IMAGE_URL' => LEPTON_URL.'/modules/lib_lepton/backend_images',		
		'TEXT' => $TEXT,		
		'MOD_PAGECLONER' => pagecloner::getInstance()->language,	
		'editable_pages' => $all_pages
	);

    $oTwig = lib_twig_box::getInstance();
	$oTwig->registerModule("pagecloner");
	
	echo $oTwig->render( 
		"@pagecloner/modify.lte",
		$pagecloner_vars
	);
}
